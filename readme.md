# JOGA+ Web

Hello Hello, friends. Our beautiful AngularJS app is build with [Yeoman](http://yeoman.io/). 

#Install

First off, you have the _app_ folder where the source of our application lies. Yet all this commands are to be executed from the root folder of our app.

To begin we need to set up our dependencies. You'll need to install [Node](http://nodejs.org/) with NPM to get everything working.

```
npm install -g yo
npm install
```

That's it for the dependencies. This installs all the tools you need. Now let's fetch all of our JS libraries.

```
bower install
```

Good, we now have everything we need. Now we need to run our server and see how our client looks like.

```
grunt server
```

If you'e lazy, just run `install.sh` in your Linux/Mac.

This won' work out of the box if you'e planning to use Facebook Connect.
You'll need to add a line to your `host file` specifying an alias for your localhost with a subdomain
allowed by the application.

```
127.0.0.1   local.herokuapp.com
```

This will open a new tab with our app. Now, isn't dandy?

## Test
Did you break something, you bloody basterd? Test it.

```
grunt test
```

## Build

Grunt builds everything for us: compiles to css, minimizes js and also copies everything into the _dist_ folder.

We deploy to Heroku, so there's a little bit of code there in our gruntfile to get things going.
We have a Procfile and a simple server with a static web server to put it online. And the _configuration.js_ inside the
_heroku_ folder defines the Facebook App ID and the server url. It isn't nice, but it's what we have when working with
html only front-end.

```
grunt build -f
```

This creates the build for Heroku (or anything you want) in the _dist_ folder, which is untracked. You can run `node server.js` and see it for yourself, Charlie Brown. Anyways, these are binaries, you're not supposed to keep track of it under source control. 

### Heroku
We don't keep track of dist but we do want to use Heroku, so we'll have a separate Git repository for Heroku where we overwrite previous releases. *We'll only commit releases*, it's the best approach we have, otherwise we'd have duplicated informations and minimized code under source control.

When we have a release all we need to do (after having set up our remote Git).

```
git push heroku master
```

Done.

