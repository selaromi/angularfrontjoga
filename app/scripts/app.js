'use strict';

var jogaApp = angular.module('angularjsJogaApp',
    [
        'LocalStorageModule',
        'facebook',
        'ngRoute',
        'ngAnimate',
        'ui.event',
        'pascalprecht.translate',
        'ngRetina',
        'angular-carousel'
    ]
);

jogaApp
    .filter('getGroup', function() {
        return function(input) {
            var groups = ['','A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];
            return groups[input];
        };
    });

jogaApp
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/home.html'
            })
            .when('/login', {
                templateUrl: 'views/login.html'
            })
            .when('/signup', {
                templateUrl: 'views/signup.html'
            })
            .when('/calendar', {
                templateUrl: 'views/calendar.html'
            })
            .when('/forgot_password', {
                templateUrl: 'views/forgot_password.html'
            })
            .when('/password_recovery/:username?/:reset_password_token?', {
                templateUrl: 'views/password_recovery.html'
            })
            .when('/ranking', {
                templateUrl: 'views/ranking.html'
            })
            .when('/me', {
                templateUrl: 'views/me.html'
            })
            .when('/user/:id', {
                templateUrl: 'views/profile.html'
            })
            .when('/league/create', {
                templateUrl: 'views/league_create.html'
            })
            .when('/league/update/:league_id', {
                templateUrl: 'views/league_update.html'
            })
            .when('/league/view/:league_id', {
                templateUrl: 'views/league_view.html'
            })
            .when('/league/view/:league_id', {
                templateUrl: 'views/league_view.html'
            })
            .when('/500', {
                templateUrl: 'views/500.html'
            })
            .otherwise({
                templateUrl: 'views/404.html'
            });
    });
