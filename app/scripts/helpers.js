function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
} 

$(function(){
    $(document).on('click','#leaderboard-result',function(){
        $(this).parent().next().children('div').animate({width:'toggle', display:'block'},350);
    }); 

    $(document).on('click','.related-results',function(){
        $(this).animate({display:'none',width:'toggle'},350);
    });  

    
});