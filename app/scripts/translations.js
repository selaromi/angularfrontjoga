var translations = {
    HEADLINE: 'What an awesome module!',
    PARAGRAPH: 'Srsly!',
    NAMESPACE: {
        PARAGRAPH: 'And it comes with awesome features!'
    }
};

jogaApp
    .config(['$translateProvider', function ($translateProvider) {
        // add translation table
        $translateProvider.translations(translations);
    }]);