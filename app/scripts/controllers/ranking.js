'use strict';

jogaApp.controller('RankingCtrl', function ($scope, $rootScope, $location, $http, $q, $timeout) {

    $scope.boxLeagueInfoExpanded = false;
    $scope.createLeague = false;
    $scope.createLeagueStep = 3;
    $scope.createLeagueProgress = 30;
    $scope.existingLeague = false;

    $scope.league_info =
        {
            name: "",
            description: ""
        };
    $scope.invitations = null;
    $scope.currentInvitation = null;
    $scope.leagues = null


    $scope.createNewLeague = function () {
        if( !$scope.createLeague ) {
            $scope.createLeagueStep = 1;
            $scope.league_info.name = "";
            $scope.league_info.description = "";
            $scope.createLeagueProgress = 30;
            $scope.existingLeague = false;
        }
        $scope.createLeague = !$scope.createLeague;

    };

    $scope.init = function() {
        if (!$rootScope.commonInit()) return;
        console.log('ligas');
        $scope.leagues = [];
        $http.post(
            $rootScope.globalSettings.SERVER+'/league/myleagues',
            {
                user:
                {
                    username: $rootScope.globalSettings.user,
                    user_token: $rootScope.globalSettings.token
                }
            })
            .error($rootScope.errorHandler)
            .then(function(response){
                var userLeagues = response.data;
                var promises = _(userLeagues).map(function(league){

                    return $http.post(
                        $rootScope.globalSettings.SERVER+'/league',
                        {
                            user:
                            {
                                username: $rootScope.globalSettings.user,
                                user_token: $rootScope.globalSettings.token
                            },
                            league:{
                                id: league.league_id,
                                page: 0
                            }
                        });
                });

                $q.all(promises)
                    .then(function(fullLeagues){
                        _(fullLeagues).each(function(response) {
                            var fullLeague = response.data;
                            $scope.leagues.push(
                                {
                                    id: fullLeague.league_info.id,
                                    name: fullLeague.league_info.name,
                                    owner: fullLeague.owner_info.username,
                                    owner_id: fullLeague.owner_info.id,
                                    users: fullLeague.league_info.users,
                                    description: fullLeague.league_info.description
                                }
                            );

                        });
                        console.dir($scope.leagues);
                        $scope.currentLeague = $scope.leagues[0];
                        console.dir($scope.leagues);

                    })
            });

        $http.post(
            $rootScope.globalSettings.SERVER+'/leagues/pendinginvitations',
            {
                user:
                {
                    username: $rootScope.globalSettings.user,
                    user_token: $rootScope.globalSettings.token
                }
            })
            .success(function(data){
                console.log(data);
                $scope.invitations = data;

                if ($scope.invitations.length > 0) $scope.currentInvitation = $scope.invitations[0];
            })
            .error($rootScope.errorHandler);
    }

    $scope.setCurrentLeaderboard = function(league) {
        if (league.id == 0) return;
        $scope.currentLeague = league;

    }

    $scope.submitCreate = function() {
        $http.post(
            $rootScope.globalSettings.SERVER+'/leagues/create',
            {
                user:
                {
                    username: $rootScope.globalSettings.user,
                    user_token: $rootScope.globalSettings.token
                },
                league:
                {
                    name: $scope.league_info.name,
                    description: $scope.league_info.description
                }
            })
            .success(function(data){
                $scope.createLeagueStep = 2;
                $scope.createLeagueProgress = 66;
                $scope.league_info.id = data.id;
            })
            .error($rootScope.errorHandler);

    }

    //Invitations.
    var invites = [];
    $scope.usersToIntive = [];
    var checkInvitesTimeoutPromise = null;
    $scope.unexistentUsersEmails = [];
    $scope.unexistentUsersNames = [];
    $scope.existentUsersEmails = [];
    $scope.existentUsersNames = [];

    $scope.checkInvites = function() {
        if(checkInvitesTimeoutPromise) {
            $timeout.cancel(checkInvitesTimeoutPromise);
        }
        checkInvitesTimeoutPromise = $timeout(function(){
            var currentUsersInInput = _($scope.invitesString
                .split(','))
                .without('')
                .map(function(name) {return name.trim()});

            //console.log(currentUsersInInput);
            var promises = _(currentUsersInInput).map(function(userToCheck){
                return $http.post(
                    $rootScope.globalSettings.SERVER+'/users/check',
                    {
                        user:
                        {
                            username: $rootScope.globalSettings.user,
                            user_token: $rootScope.globalSettings.token
                        },
                        invited_user: validateEmail(userToCheck) ? { email: userToCheck } : { username: userToCheck }
                    })
            });
            $q.all(promises).then(function(responses){
                var values = _(responses).map(function(response){ return response.data.exists })
                var unexistentUsersEmails = [];
                var unexistentUsersNames = [];
                var existentUsersEmails = [];
                var existentUsersNames = [];


                _(values).each(function(exists, index) {
                    if (!exists) {
                        if (!validateEmail(currentUsersInInput[index])) {
                            unexistentUsersNames.push(currentUsersInInput[index]);
                        } else {
                            unexistentUsersEmails.push(currentUsersInInput[index]);
                        }
                    } else {
                        if (!validateEmail(currentUsersInInput[index])) {
                            existentUsersNames.push(currentUsersInInput[index]);
                        } else {
                            existentUsersEmails.push(currentUsersInInput[index]);
                        }
                    }
                });
                $scope.unexistentUsersEmails = unexistentUsersEmails;
                $scope.unexistentUsersNames = unexistentUsersNames;

                $scope.existentUsersEmails = existentUsersEmails;
                $scope.existentUsersNames = existentUsersNames;
            });

        }, 1000);
    };

    $scope.invite = function() {
        $http.post(
            $rootScope.globalSettings.SERVER+'/leagues/invite',
            {
                user:
                {
                    username: $rootScope.globalSettings.user,
                    user_token: $rootScope.globalSettings.token
                },
                league:
                {
                    id: $scope.league_info.id
                },
                invitations:
                {
                    other: $scope.existentUsersEmails.concat($scope.existentUsersNames)
                                                     .concat($scope.unexistentUsersEmails)
                                                     .join(),
                    facebook: ""
                }
            })
            .success(function(data){
                //console.log(data);
                if($scope.existingLeague)
                {
                    $location.path("/ranking");
                    $scope.createLeague = false;
                    return;
                }
                $scope.createLeagueStep = 3;
                $scope.createLeagueProgress = 100;
                $timeout(function(){
                    $scope.createLeague = false;
                    $location.path("/ranking");
                },10000);
            })
            .error($rootScope.errorHandler);

    };

    $scope.inviteToLeague = function(){
        $scope.league_info = $scope.currentLeague;
        $scope.createLeague = true;
        $scope.createLeagueStep = 2;
        $scope.existingLeague = true;
    }

    $scope.prevInvitation = function(invitation){
        var index = $scope.invitations.indexOf(invitation)-1;
        if(index < 0) index = $scope.invitations.length -1;
        $scope.currentInvitation = $scope.invitations[index];
    }

    $scope.nextInvitation = function(invitation){
        $scope.currentInvitation = $scope.invitations[($scope.invitations.indexOf(invitation)+1)%$scope.invitations.length];

    }

    $scope.respondInvitation = function(currentInvitationLeague,response) {
//        console.log(currentInvitationLeague);
        $http.post(
            $rootScope.globalSettings.SERVER+'/leagues/respondinvitation',
            {
                user:
                {
                    username: $rootScope.globalSettings.user,
                    user_token: $rootScope.globalSettings.token,
                    user_response: response
                },
                league:
                {
                    id: currentInvitationLeague.id
                }
            })
            .success(function(data){
                console.log(data);
                console.log($scope.invitations.indexOf($scope.currentInvitation));
                console.log($scope.invitations);
                $scope.invitations.splice($scope.invitations.indexOf($scope.currentInvitation),1);
                console.log($scope.invitations);
                if( $scope.invitations.length == 0)
                {
                    $scope.currentInvitation = null;
                } else {
                    $scope.currentInvitation = $scope.invitations[0];
                }
                $location.path("/ranking");

            })
            .error($rootScope.errorHandler);

    };

    $scope.closeCreateLeague = function(){
        $scope.createLeague = false;
        $location.path("/ranking");
    }

    $scope.expandBoxLeagueInfo = function(){
        $scope.boxLeagueInfoExpanded = !$scope.boxLeagueInfoExpanded;
    }

});