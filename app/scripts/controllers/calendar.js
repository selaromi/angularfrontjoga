'use strict';

jogaApp.controller('CalendarCtrl', function ($scope, $rootScope, $location, $http, $timeout) {
    //an object for storing promises when the user clicks

    $scope.init = function () {
        $scope.user = {};
        $scope.selectedGroup = 1;
        if ($rootScope.globalSettings.token === null) {
            $location.path("/login");
        }
        loadGroup();
    };

    function loadGroup() {
        $http.post(
            $rootScope.globalSettings.SERVER + '/user/me',
            {
                user: {
                    username: $rootScope.globalSettings.user,
                    user_token: $rootScope.globalSettings.token
                }
            })
            .success(function (data) {
                $scope.user = data;
                //now we get the matches.
                $http.post($rootScope.globalSettings.SERVER + '/matches')
                    .success(function (matches) {
                        $scope.matches = matches;
                        //now the stadia.
                        $http.post($rootScope.globalSettings.SERVER + '/stadia')
                            .success(function (stadia) {
                                $scope.stadia = stadia;
                                $scope.selectGroups($scope.selectedGroup);
                            })
                            .error($rootScope.errorHandler);
                    })
                    .error($rootScope.errorHandler);
            })
            .error($rootScope.errorHandler);
    }

    //Changes the prediction in the calendar view.
    $scope.changePredictionCalendar = function(predictionID, operation, team) {
        $rootScope.changePrediction(predictionID, $scope.user.predictions, operation, team);
    };

    $scope.createPrediction = function(matchID) {
        $http.post(
            $rootScope.globalSettings.SERVER + '/predictions/create',
            {
                user:
                {
                    username: $rootScope.globalSettings.user,
                    user_token: $rootScope.globalSettings.token
                },
                matches:[{
                    match_id: matchID,
                    local_goals: 0,
                    visitor_goals: 0,
                    winner_team: 0
                }]
            })
            .success(function(data){
                loadGroup();
            })
            .error($rootScope.errorHandler);
    };

    //Displayes the matches depending on the group.
    $scope.selectGroups = function (group_id) {
        $scope.selectedGroup = group_id;
        var matchesForGroup = _($scope.matches).where({group_id: group_id});
        _(matchesForGroup).each(function (match, indexOfMatch) { //we fetch the prediction if there' one.
            _($scope.user.predictions).each(function (prediction, index) {
                if (match.id == prediction.match_id) {
                    matchesForGroup[indexOfMatch].prediction = $scope.user.predictions[index];
                }
            });
            match.stadium = _($scope.stadia).findWhere({id: match.stadium_id}).name;
        });
        $scope.matchesForGroup = matchesForGroup;
    };
});