'use strict';

jogaApp.controller('HomeCtrl', function ($scope, $rootScope, $location, $http) {
    $scope.init = function(){
        $scope.user ={};
        $scope.badges = [1,2,3,4,5,6,7,8,9,10];
        if($rootScope.globalSettings.token === null) {
            $location.path("/login");
        }
        $http.post(
            $rootScope.globalSettings.SERVER+'/user/me',
            {
                user:
                {
                    username: $rootScope.globalSettings.user,
                    user_token: $rootScope.globalSettings.token
                }
            })
            .success(function(data){
                $scope.user = data;
            })
            .error($rootScope.errorHandler);

        $http.post(
            $rootScope.globalSettings.SERVER+'/leagues/globalstanding',
            {
                user:
                {
                    username: $rootScope.globalSettings.user,
                    user_token: $rootScope.globalSettings.token
                }
            }

        )
        .success(function(data){
            console.log('data:');
            console.log(data);
            $scope.leaderboard = data;

        })
        .error($rootScope.errorHandler);
    };

});