'use strict';

jogaApp.controller('LoginCtrl', function ($scope, $rootScope, $location, $http, localStorageService, Facebook, $routeParams, $timeout) {
    $scope.username = 'ignacio.maldonado@gmail.com';
    $scope.password = 'quwerty123';
    $scope.newPassword = '';
    $scope.messageSent = false;
    $scope.passwordChangeSuccess = false;

    $scope.init = function () {
        if ($rootScope.globalSettings.token !== null) {
            $location.path("/");
        }
    };

    $scope.tryLoginWithFacebook = function () {
        Facebook.getLoginStatus(function (response) {
            console.log(response)
            if (response.status == 'connected') {
                ///users/auth/facebook/callback?signed_request='FB_signed_request'
                $http.post(
                    $rootScope.globalSettings.SERVER + "/users/auth/facebook/callback?signed_request="+response.authResponse.signedRequest)
                    .success(function (data, status, headers, config) {
                        $rootScope.globalSettings.token = data.auth_token;
                        $rootScope.globalSettings.user = data.username;
                        localStorageService.add(CLIENT+'.user', data.username);
                        localStorageService.add(CLIENT+'.token', data.auth_token);
                        $location.path("/");
                    })
                    .error($rootScope.errorHandler);
            }
            else
                $scope.loginWithFacebook();
        });
    };

    /**
     * Login
     */
    $scope.loginWithFacebook = function () {
        Facebook.login(function (response) {
            if (response.status == 'connected') {
                $http.post(
                    $rootScope.globalSettings.SERVER + "/users/auth/facebook/callback?signed_request="+response.authResponse.signedRequest)
                    .success(function (data, status, headers, config) {
                        $rootScope.globalSettings.token = data.auth_token;
                        $rootScope.globalSettings.user = data.username;
                        localStorageService.add(CLIENT+'.user', data.username);
                        localStorageService.add(CLIENT+'.token', data.auth_token);
                        localStorageService.add(CLIENT+'.dif', data.uid);
                        $location.path("/");
                    })
                    .error($rootScope.errorHandler);
            }

        }, {scope: 'email,publish_actions'});
    };

    $scope.me = function () {
        Facebook.api('/me', function (response) {
            /**
             * Using $scope.$apply since this happens outside angular framework.
             */
            console.dir(response);

        });
    };

    $scope.login = function () {
        console.log('login');
        console.log($rootScope.globalSettings.SERVER)
        $http.post(
            $rootScope.globalSettings.SERVER + '/users/login',
            {
                user: {username: $scope.username, password: $scope.password}
            })
            .success(function (data, status, headers, config) {
                $rootScope.globalSettings.token = data.auth_token;
                $rootScope.globalSettings.user = data.username;
                localStorageService.add(CLIENT+'.user', data.username);
                localStorageService.add(CLIENT+'.token', data.auth_token);
                localStorageService.add(CLIENT+'.fid', data.uid);
                $location.path("/");
            })
            .error($rootScope.errorHandler);
    }

    $scope.forgotPassword = function () {
        if( !$scope.messageSent ) {
            $http.post(
                $rootScope.globalSettings.SERVER+"/users/forgotpass",
                {
                    user: {email: $scope.email}
                })
                .success(function (data, status, headers, config){
                    console.log("token asked for");
                    console.log(data);
                    $scope.messageSent = true;
                })
                .error($rootScope.errorHandler);
        }
    };

    $scope.recoverPassword = function () {
//        console.log($scope.newPassword);
//        console.log($routeParams);
        if( !$scope.passwordChangeSuccess ) {
            $http.post(
                $rootScope.globalSettings.SERVER+"/users/resetpass",
                {
                    user: {
                        reset_password_token: $routeParams.reset_password_token,
                        password: $scope.newPassword,
                        username: $routeParams.username
                    }
                })
                .success(function (data, status, headers, config){
                    console.log("New password set.");
                    console.log(data);
                    $scope.passwordChangeSuccess = true;
                    $timeout(function(){
                        $location.path("/");
                    }, 3000);


                })
                .error($rootScope.errorHandler);
        }
    };
});