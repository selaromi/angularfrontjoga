'use strict';

jogaApp.controller('ProfileCtrl', function ($scope, $rootScope, $location, $http, $routeParams) {
    $scope.init = function(){
        console.log($routeParams);
        $scope.user ={};
        if($rootScope.globalSettings.token === null) {
            $location.path("/login");
        }
        $http.post(
            $rootScope.globalSettings.SERVER+'/user/me',
            {
                user:
                {
                    username: $rootScope.globalSettings.user,
                    user_token: $rootScope.globalSettings.token
                }
            })
            .success(function(data){
                console.log('perfil');
                console.log(data);
                $scope.user = data;
            })
            .error($rootScope.errorHandler);

    }
});