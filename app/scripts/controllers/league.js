'use strict';

jogaApp.controller('LeagueCtrl', function ($scope, $rootScope, $location, $http, $routeParams, $timeout, $q) {
    $scope.invitesString = "macayaven@gmail.com, ignacio.maldonado@gmail.com";
    $scope.init = function(){
        if($rootScope.globalSettings.token === null) {
            $location.path("/login");
        }
        if($routeParams.league_id) {
            $scope.league_id = $routeParams.league_id;
            $http.post(
                $rootScope.globalSettings.SERVER+'/league',
                {
                    user:
                    {
                        username: $rootScope.globalSettings.user,
                        user_token: $rootScope.globalSettings.token
                    },
                    league:
                    {
                        id: $scope.league_id,
                        page: 0
                    }
                })
                .success(function(data){
                    console.log("Liga:");
                    console.log(data);
                    $scope.league_info = data.league_info;
                })
                .error($rootScope.errorHandler);
        }
    }

    $scope.submitCreate = function() {
        $http.post(
            $rootScope.globalSettings.SERVER+'/leagues/create',
            {
                user:
                {
                    username: $rootScope.globalSettings.user,
                    user_token: $rootScope.globalSettings.token
                },
                league:
                {
                    name: $scope.league_info.name,
                    description: $scope.league_info.description
                }
            })
            .success(function(data){
                console.log(data);
                $location.path("/ranking");
            })
            .error($rootScope.errorHandler);

    }

    $scope.submitUpdate = function() {
        $http.post(
            $rootScope.globalSettings.SERVER+'/leagues/update',
            {
                user:
                {
                    username: $rootScope.globalSettings.user,
                    user_token: $rootScope.globalSettings.token
                },
                league:
                {
                    id: $scope.league_id,
                    name: $scope.league_info.name,
                    description: $scope.league_info.description
                }
            })
            .success(function(data){
                console.log(data);
                $location.path("/ranking");
            })
            .error($rootScope.errorHandler);

    };

    //Invitations.
    var invites = [];
    $scope.usersToIntive = [];
    var checkInvitesTimeoutPromise = null;
    $scope.unexistentUsersEmails = [];
    $scope.unexistentUsersNames = [];
    $scope.existentUsersEmails = [];
    $scope.existentUsersNames = [];

    $scope.checkInvites = function() {
        if(checkInvitesTimeoutPromise) {
            $timeout.cancel(checkInvitesTimeoutPromise);
        }
        checkInvitesTimeoutPromise = $timeout(function(){
            var currentUsersInInput = _($scope.invitesString
                                        .split(','))
                                        .without('')
                                        .map(function(name) {return name.trim()});

            console.log(currentUsersInInput);
            var promises = _(currentUsersInInput).map(function(userToCheck){
                return $http.post(
                    $rootScope.globalSettings.SERVER+'/users/check',
                    {
                        user:
                        {
                            username: $rootScope.globalSettings.user,
                            user_token: $rootScope.globalSettings.token
                        },
                        invited_user: validateEmail(userToCheck) ? { email: userToCheck } : { username: userToCheck }
                    })
            });
            $q.all(promises).then(function(responses){
                var values = _(responses).map(function(response){ return response.data.exists })
                var unexistentUsersEmails = [];
                var unexistentUsersNames = [];
                var existentUsersEmails = [];
                var existentUsersNames = [];


                _(values).each(function(exists, index) {
                    if (!exists) {
                        if (!validateEmail(currentUsersInInput[index])) {
                            unexistentUsersNames.push(currentUsersInInput[index]);
                        } else {
                            unexistentUsersEmails.push(currentUsersInInput[index]);
                        }
                    } else {
                        if (!validateEmail(currentUsersInInput[index])) {
                            existentUsersNames.push(currentUsersInInput[index]);
                        } else {
                            existentUsersEmails.push(currentUsersInInput[index]);
                        }
                    }
                });
                $scope.unexistentUsersEmails = unexistentUsersEmails;
                $scope.unexistentUsersNames = unexistentUsersNames;

                $scope.existentUsersEmails = existentUsersEmails;
                $scope.existentUsersNames = existentUsersNames;
            });

        }, 1000);
    };

    $scope.invite = function() {
        //todo: invitar.
    };
});