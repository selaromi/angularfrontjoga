'use strict';

jogaApp.controller('MeCtrl', function ($scope, $rootScope, $location, $http, $timeout) {
    //an object for storing promises when the user clicks
    var predictionPromises = {};

    $scope.mainLeaderboard = [];

    $scope.init = function(){
        $scope.user =null;
        if($rootScope.globalSettings.token === null) {
            $location.path("/login");
        }
        $http.post(
            $rootScope.globalSettings.SERVER+'/user/me',
            {
                user:
                    {
                        username: $rootScope.globalSettings.user,
                        user_token: $rootScope.globalSettings.token
                    }
            })
            .success(function(data){
                console.log('perfil');
                console.log(data);
                $scope.user = data;
                console.dir(data);
            })
            .error($rootScope.errorHandler);

        $http.post(
            $rootScope.globalSettings.SERVER+'/leagues/globalstanding',
            {
                user:
                {
                    username: $rootScope.globalSettings.user,
                    user_token: $rootScope.globalSettings.token
                }
            }

        )
            .success(function(data){
                console.log(data);
                $scope.mainLeaderboard = data;

            })
            .error($rootScope.errorHandler);

    };
});