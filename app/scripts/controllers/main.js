'use strict';

jogaApp.run(function ($rootScope, $http, $location, localStorageService) {
    $rootScope.globalSettings = {
        user: localStorageService.get(CLIENT + '.user'),
        token: localStorageService.get(CLIENT + '.token'),
        fid: localStorageService.get(CLIENT + '.fid'),
        pts: null,
        SERVER: SERVER_URL
    }

    $rootScope.notification = "";

    //matches
    $http.post(
            $rootScope.globalSettings.SERVER + '/matches')
        .success(function (data) {
            //todo: use matches.
        })
        .error($rootScope.errorHandler);

    //stadia
    $http.post(
            $rootScope.globalSettings.SERVER + '/stadia')
        .success(function (data) {
            //todo: use stadia.
        })
        .error($rootScope.errorHandler);

    //teams!
    $http.post(
            $rootScope.globalSettings.SERVER + '/teams')
        .success(function (data) {
            //todo: use teams.
        })
        .error($rootScope.errorHandler);

    $rootScope.$watch(function () {
        localStorageService.add(CLIENT + '.user', $rootScope.globalSettings.user);
        localStorageService.add(CLIENT + '.token', $rootScope.globalSettings.token);
    })
});

jogaApp.controller('MainCtrl', function ($scope, $rootScope, $http, $location, $timeout) {
    var predictionPromises = {};

    $scope.logout = function () {
        console.log('logout!');
        $location.path("/");
        $rootScope.globalSettings.user = null;
        $rootScope.globalSettings.token = null;
        $location.path("/login");
    }

    $rootScope.errorHandler = function (data, status, headers, config) {
        console.log("error")
        console.dir(status);
        switch (status) {
            case NOT_FOUND:
                $location.path("/404");
                break;
            case NOT_AUTHORIZED:
                $rootScope.globalSettings.user = null;
                $rootScope.globalSettings.token = null;
                $location.path("/login");
                break;
            case SERVER_ERROR:
                $location.path("/500");
                break;
            default:
                break;
        }

    };


    $rootScope.changePrediction = function (predictionID, predictions, operation, team) {
        var prediction = _(predictions).findWhere({id: predictionID});
        if (team == VISITOR) {
            prediction.visitor_goals += (operation == ADD) ? 1 : -1;
            prediction.visitor_goals =
                prediction.visitor_goals < 0 ? 0 : prediction.visitor_goals;
        } else {
            prediction.local_goals += (operation == ADD) ? 1 : -1;
            prediction.local_goals =
                prediction.local_goals < 0 ? 0 : prediction.local_goals;
        }
        if (predictionPromises[predictionID]) {
            var timeoutPromise = predictionPromises[predictionID];
            $timeout.cancel(timeoutPromise);
        }

        predictionPromises[predictionID] = $timeout(function () {
            console.log("Saving prediction.");
            updatePrediction(prediction);
        }, 2000)

    };

    /**
     * Updates a prediction from a prediction object.
     * @param prediction
     */
    function updatePrediction(prediction) {
        $http.post(
            $rootScope.globalSettings.SERVER+'/predictions/update',
            {
                user:
                {
                    username: $rootScope.globalSettings.user,
                    user_token: $rootScope.globalSettings.token
                },
                prediction:
                {
                    id: prediction.id,
                    local_goals: prediction.local_goals,
                    visitor_goals: prediction.visitor_goals,
                    winner_team: prediction.winner_team
                }
            })
            .success(function(data){
                console.log("prediction saved");
                $rootScope.setNotification("prediction saved!");
            })
            .error($rootScope.errorHandler);
    }

    /**
     * For now, it checks whether the user is logged in or not and redirects. Just a pattern.
     * @returns {boolean}
     */
    $rootScope.commonInit = function () {
        var loggedIn = true;
        if ($rootScope.globalSettings.token === null) { //there has to be a token o access this place.
            $location.path("/login");
            loggedIn = false;
        }
        return loggedIn;
    }

    $rootScope.setNotification = function (message) {
        $rootScope.notification = message;
        $timeout(function () {
            $rootScope.notification = "";
        }, 500);
    }

    $rootScope.isLocation = function (location) {
        return location == $location.path();
    }

});

