'use strict';

jogaApp.controller('SignupCtrl', function ($scope, $rootScope, $location, $http) {
    $scope.signin = function() {
        console.log($scope.user);
        $http.post(
            $rootScope.globalSettings.SERVER+'/users/register',
            {
                user:
                    {
                        firstName: $scope.user.firstName,
                        lastName: $scope.user.lastName,
                        username: $scope.user.username,
                        email: $scope.user.email,
                        mobile: $scope.user.mobile,
                        password: $scope.user.password,
                        password_confirmation: $scope.user.password_confirmation
                    }
            })
            .success(function(data, status, headers, config) {
                console.log(data);
                $rootScope.globalSettings.token = data.auth_token;
                $rootScope.globalSettings.user = data.username;
                $location.path("/");
            })
            .error(function(data, status, headers, config) {
                console.log("error, baby")
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
    }
});